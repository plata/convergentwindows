The **Convergent Windows** KWin script manages windows on mobile devices with or without external screen.

## Features
- Remove window decorations and maximize windows on the first screen (i.e. mobile device).
- Show window decorations and generally use the default behavior on all other screens (i.e. external screens).

## Known Limitations
- Some GTK apps draw the titlebar buttons inside the window. Those are still visible.
